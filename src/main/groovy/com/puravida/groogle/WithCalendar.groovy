package com.puravida.groogle

import java.util.function.Consumer

interface WithCalendar {

    String getId()

    String getSummary()

    void setSummary(String summary)

    String getDescription()

    void setDescription(String description)

    String getLocation()

    void setLocation(String location)

    String getTimeZone()

    void setTimeZone(String timeZone)

    String newEvent(Consumer<WithEvent> consumer)

    void deleteEvent( String id)

    int eachEventInCalendar(Consumer<WithExistingEvent> consumer)
}
