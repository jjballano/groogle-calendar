package com.puravida.groogle

import com.google.api.services.calendar.model.Calendar
import com.google.api.services.calendar.model.CalendarListEntry

import java.util.function.Consumer

interface CalendarService extends Groogle.GroogleService{

    String[]getCalendarIds()

    int eachCalendarInList( Consumer<WithExistingCalendar> consumer)

    void withPrimaryCalendar( Consumer<WithExistingCalendar>consumer)

    void withCalendar( String calendarId, Consumer<WithExistingCalendar>consumer)

    String newCalendar(Consumer<WithCalendar>consumer)

    void deleteCalendar(String calendarId)
}