package com.puravida.groogle.impl

import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.Event
import com.google.api.services.calendar.model.Events
import com.puravida.groogle.CalendarService
import com.puravida.groogle.WithEvent
import com.puravida.groogle.WithExistingCalendar
import com.puravida.groogle.WithExistingEvent

import java.util.function.Consumer

class WithCalendarImpl implements WithExistingCalendar{

    Calendar service
    com.google.api.services.calendar.model.Calendar calendar

    boolean toCreate = false

    boolean toDelete = false

    boolean dirty = false

    @Override
    String getId() {
        return calendar.id
    }

    @Override
    void delete() {
        toDelete = true
    }

    @Override
    String getSummary() {
        calendar.getSummary()
    }

    @Override
    void setSummary(String summary) {
        calendar.setSummary(summary)
        dirty = true
    }

    @Override
    String getDescription() {
        calendar.getDescription()
    }

    @Override
    void setDescription(String description) {
        calendar.setDescription(description)
        dirty = true
    }

    @Override
    String getLocation() {
        calendar.getLocation()
    }

    @Override
    void setLocation(String location) {
        calendar.setLocation(location)
        dirty = true
    }

    @Override
    String getTimeZone() {
        calendar.getTimeZone()
    }

    @Override
    void setTimeZone(String timeZone) {
        calendar.setTimeZone(timeZone)
        dirty = true
    }

    void execute(){
        if( toDelete ){
            service.calendars().delete(calendar.id).execute()
        }else{
            if( dirty ){
                if( toCreate ){
                    calendar = service.calendars().insert(calendar).execute()
                }else {
                    service.calendars().update(calendar.id, calendar).execute()
                }
            }
        }
    }

    @Override
    String newEvent(Consumer<WithEvent> consumer) {
        Event event = new Event()
        WithEventImpl impl = new WithEventImpl(service:service, calendar: calendar, event:event, toCreate: true)
        consumer.accept(impl)
        impl.execute()
        impl.event.id
    }

    @Override
    void deleteEvent(String eventId) {
        service.events().delete(calendar.id, eventId).execute()
    }

    @Override
    int eachEventInCalendar(Consumer<WithExistingEvent> consumer) {
        int count=0
        Events events = service.events().list(calendar.id).execute()
        while( true ) {
            events.items.each {
                count++
                WithEventImpl impl = new WithEventImpl(service: service, calendar:calendar, event:it)
                consumer.accept(impl)
            }
            if( !events.nextPageToken )
                break
            events = this.service.events().list(calendar.id).setPageToken(events.nextPageToken).execute()
        }
        count
    }
}
