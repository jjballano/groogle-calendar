package com.puravida.groogle.impl

import com.google.api.client.util.DateTime
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.Event
import com.google.api.services.calendar.model.EventDateTime
import com.puravida.groogle.EventStatus
import com.puravida.groogle.WithEvent
import com.puravida.groogle.WithExistingEvent

import java.text.SimpleDateFormat

class WithEventImpl implements WithExistingEvent{

    Calendar service

    com.google.api.services.calendar.model.Calendar calendar

    Event event

    boolean toCreate = false

    boolean toDelete = false

    boolean dirty = false

    @Override
    String getSummary() {
        event.summary
    }

    @Override
    WithEvent setSummary(String summary) {
        event.summary = summary
        dirty = true
        this
    }

    @Override
    String getDescription() {
        event.description
    }

    @Override
    WithEvent setDescription(String description) {
        event.description = description
        dirty = true
        this
    }

    @Override
    EventStatus getStatus(){
        EventStatus status = EventStatus.findByKey(event.status)
        if (!status){
            return
        }
        status
    }

    @Override
    Date getStart() {
        EventDateTime eventStart = event.getStart() ?: event.getOriginalStartTime()
        DateTime dateTime = eventStart.getDateTime() ?: eventStart.getDate()
        new Date(dateTime.getValue())
    }

    @Override
    WithEvent setStartDay(Date d) {
        event.start = new EventDateTime(date: new DateTime(d.format("yyyy-MM-dd", TimeZone.getTimeZone(calendar.timeZone))))
        dirty = true
        this
    }


    @Override
    WithEvent setStart(Date d) {
        event.start = new EventDateTime(dateTime: new DateTime(d.time) )
        dirty = true
        this
    }

    @Override
    Date getEnd() {
        if (event.isEndTimeUnspecified() || !event.getEnd()){
            return  null
        }
        DateTime dateTime = event.getEnd().getDateTime() ?: event.getEnd().getDate()
        new Date(dateTime.getValue())
    }

    @Override
    WithEvent setEnd(Date d) {
        event.end = new EventDateTime(dateTime: new DateTime(d.time) )
        dirty = true
        this
    }


    @Override
    WithEvent setEndDay(Date d) {
        event.end = new EventDateTime(date: new DateTime(d.format("yyyy-MM-dd", TimeZone.getTimeZone(calendar.timeZone))))
        dirty = true
        this
    }

    @Override
    WithEvent moveTo(Date d) {
        moveTo( new SimpleDateFormat('yyyyMMdd').format(d))
    }

    @Override
    WithEvent moveTo(String date) {
        event.end=event.start=new EventDateTime(date: new DateTime(date))
        dirty=true
        this
    }

    @Override
    WithExistingEvent delete() {
        toDelete =true
        this
    }

    void execute(){
        if( toDelete ){
            service.events().delete(calendar.id, event.id).execute()
        }else{
            if( dirty ){
                if( toCreate ){
                    event = service.events().insert(calendar.id, event).execute()
                }else {
                    service.events().update(calendar.id, event.id, event).execute()
                }
            }
        }
    }
}
