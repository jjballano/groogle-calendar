package com.puravida.groogle

interface WithExistingEvent extends WithEvent{

    WithExistingEvent delete()

}