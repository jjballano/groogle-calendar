= Groogle Calendar
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle Calendar DSL
:keyworks: groovy, google, groogle, dsl, bigquery

Groogle Calendar es el DSL que nos permitirá interactuar con los calendarios y eventos de un usuario

== Maven/Gradle

Para añadir las dependencias necesarias a tu proyecto usa las siguientes coordenadas:

.pom.xml
[#maven,source,xml]
----
<dependency>
  <groupId>com.puravida.groogle</groupId>
  <artifactId>groogle-calendar</artifactId>
  <version>{lastVersion}</version>
  <type>pom</type>
</dependency>
----

.build.gradle
[#gradle,source,groovy]
----
compile 'com.puravida.groogle:groogle-calendar:{lastVersion}'
----

== Registro

Para poder usar CalendarService en un entorno `autentificado` debemos registrar el servicio durante el
proceso de construcción de `groogle`:

[#registerjava,source,groovy]
----
include::{examplesdir}/com/puravida/groogle/BasicGroogleTest.groovy[tags=register]
----
<1> Registramos un servicio Calendar en Groogle


WARNING: En el registro de Groogle sólo puede haber un servicio de cada tipo (clase)


== Uso

Una vez que el servicio(s) está registrado podemos acceder al mismo en cualquier parte de nuestro
programa a través del objeto `groogle` obtenido:

[#example,source,groovy]
----
include::{examplesdir}/com/puravida/groogle/BasicGroogleTest.groovy[tags=service]
----

